package au.id.tjsr.logging.jdbc;

import java.util.logging.Level;
import java.util.logging.Logger;

public class TestJDBCLogHandler {

	/**
	 * Set the following string to whatever JDBC driver you wish to use. I have it
	 * set to use the MM driver of MySQL. You can get the MM driver from:
	 * http://mmmysql.sourceforge.net/.
	 */
	static public final String driver = "org.gjt.mm.mysql.Driver";

	/**
	 * Set this to your connection string to access your database. Refer to your
	 * database documentation on how to set this. The one I have below logs into a
	 * MySQL database.
	 */
	static public final String connection = "jdbc:mysql://192.168.1.100/logging"
			+ "?user=logger&password=logpass&database=logging";

	/**
	 * Main function. Performs some basic log testing.
	 * 
	 * @param argv
	 *            Arguments are not used.
	 */

	public static void main(String argv[]) {
		// set up the JDBCLogger handler
		JDBCLogHandler jdbcHandler = new JDBCLogHandler(driver, connection);
		jdbcHandler.clear();

		// setup
		Logger logger = Logger.getLogger("com.heaton.articles.logger");
		logger.addHandler(jdbcHandler);
		logger.setLevel(Level.ALL);

		// try some logging

		logger.info("Sample log entry");

		logger.warning("Sample warning");

		try {
			int i = 0 / 0;
		} catch (Exception e) {
			logger.log(Level.WARNING, "This is what an exception looks like", e);
		}
	}
}